<?php

namespace Drupal\japanese_era_formatter\Enum;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Enum for era.
 */
enum Era: string {
  case Meiji = 'M';
  case Taisho = 'T';
  case Showa = 'S';
  case Heisei = 'H';
  case Reiwa = 'R';

  /**
   * Get start date.
   */
  public function getStartDate(): string {
    return match($this) {
      self::Meiji => '1873-01-01',
      self::Taisho => '1912-07-30',
      self::Showa => '1926-12-25',
      self::Heisei => '1989-01-08',
      self::Reiwa => '2019-05-01',
    };
  }

  /**
   * Get japanese name.
   */
  public function getJapaneseName(): string {
    return match($this) {
      self::Meiji => '明治',
      self::Taisho => '大正',
      self::Showa => '昭和',
      self::Heisei => '平成',
      self::Reiwa => '令和',
    };
  }

  /**
   * Get era from DrupalDatetime object.
   */
  public static function fromDateTime(DrupalDateTime $date): ?self {
    foreach (array_reverse(self::cases()) as $case) {
      if ($date >= $case->getStartDate()) {
        return $case;
      }
    }
    // If the date does not belong to any era.
    return NULL;
  }

  /**
   * Gets the number of years since the beginning of the era name.
   */
  public function yearSinceStartEra($datetime): int {
    $startdate = new DrupalDateTime($this->getStartDate());
    return $startdate->diff($datetime)->y + 1;
  }

}
